#! /bin/sh

set -e

results=results_sexp.txt
results_blake=results_blake.txt

benchmark_hacl_version () {
    local bench_version=$1
    local hacl_version=$2
    local pin_path=$3
    local display_version=$hacl_version
    opam uninstall -y hacl-star-raw
    opam clean
    if [ "$pin_path" = "" ]; then
	      opam install -y hacl-star.$hacl_version
    else
        opam unpin -y hacl-star-raw hacl-star
        opam pin -y $pin_path
    fi
	  eval $(opam env --shell=sh)
    echo "((hacl_version $display_version) " >> $results
	  dune exec ./bench_$bench_version/bench.exe >> $results
    echo ")" >> $results
    opam unpin -n hacl-star hacl-star-raw
}


rm -f $results $results_blake
opam update

# Latest version
benchmark_hacl_version 06 0.7.0

# Finer-grained Blake2b benchmarks using latest hacl-star
dune exec ./bench_blake/bench.exe >> $results_blake

# Previous versions
benchmark_hacl_version 06 0.6.2
benchmark_hacl_version 06 0.6.1
benchmark_hacl_version 06 0.6.0
benchmark_hacl_version 04 0.5.0
benchmark_hacl_version 04 0.4.5
benchmark_hacl_version 04 0.4.4
benchmark_hacl_version 04 0.4.3
benchmark_hacl_version 04 0.4.2
benchmark_hacl_version 04 0.4.1
benchmark_hacl_version 03 0.3.2
benchmark_hacl_version 03 0.3.0-1

dune exec ./utils/analyse.exe
