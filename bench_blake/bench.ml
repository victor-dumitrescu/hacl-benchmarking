(* The MIT License (MIT)
 *
 *   Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE. *)

open Core_bench
open Hacl_star

let gen size =
  match Hacl.RandomBuffer.randombytes ~size with
  | Some b -> b
  | None -> failwith "Error getting random bytes"

let msgs = List.map gen [ 0; 100; 1000; 10000; 100000 ]

let digestif_hashes =
  List.map (fun msg -> Digestif.BLAKE2B.digest_bytes msg) msgs

let hacl_hashes =
  List.map (fun msg -> Hacl.Blake2b_32.hash ~key:Bytes.empty msg 64) msgs

let blake2b_digestif =
  Bench.Test.create_group ~name:"Digestif/BLAKE2B.digest_bytes"
    (List.map
       (fun msg ->
         let name = Int.to_string (Bytes.length msg) in
         Bench.Test.create ~name (fun () ->
             ignore Digestif.BLAKE2B.(to_raw_string (digest_bytes msg))))
       msgs)

let blake2b_hacl =
  let tests =
    (if AutoConfig2.(has_feature VEC256) then
     [ ("Blake2b_256.hash", Hacl.Blake2b_256.hash) ]
    else [])
    @ [ ("Blake2b_32.hash", Hacl.Blake2b_32.hash) ]
  in
  Bench.Test.create_group ~name:"Hacl"
    (List.map
       (fun (name, (hash_function : ?key:Bytes.t -> Bytes.t -> int -> Bytes.t)) ->
         Bench.Test.create_group ~name
           (List.map
              (fun msg ->
                let name = Int.to_string (Bytes.length msg) in
                Bench.Test.create ~name (fun () ->
                    ignore (hash_function msg 64)))
              msgs))
       tests)

let tests = [ blake2b_digestif; blake2b_hacl ]

let rec check_digests h1 h2 =
  let hex m = Hex.show (Hex.of_bytes m) in
  match (h1, h2) with
  | hd1 :: tl1, hd2 :: tl2 ->
      let digestif_hash = Digestif.BLAKE2B.to_hex hd1 in
      let hacl_hash = hex hd2 in
      assert (digestif_hash = hacl_hash);
      check_digests tl1 tl2
  | [], [] -> ()
  | _ -> assert false

let main () =
  check_digests digestif_hashes hacl_hashes;
  let open Bench in
  let run_config =
    Run_config.create ~verbosity:Quiet ~quota:(Quota.of_string "10") ()
  in
  let measurements = measure ~run_config tests in
  let analyses =
    List.map
      (fun m ->
        match analyze m with
        | Ok a -> a
        | Error _ -> failwith "Analysis failure")
      measurements
  in
  let display_config = Display_config.create () in
  display ~display_config analyses

let () =
  Gc.(
    set { (get ()) with allocation_policy = 2 (* Use the best-fit strategy *) });
  main ()
