(* The MIT License (MIT)
 *
 *   Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE. *)

open Core_bench
open Hacl_star

let hash_msg_size = 10000
let sign_msg_size = 256

let gen size =
  let write buf =
    if Hacl.RandomBuffer.randombytes buf then ()
    else failwith "Error getting random bytes"
  in
  let buf = Bytes.create size in
  write buf;
  buf

let blake2b_tests =
  let digest = Bytes.create 64 in
  let tests =
    [ ("32", Hacl.Blake2b_32.hash) ]
    @
    if AutoConfig2.(has_feature AVX2) then [ ("256", Hacl.Blake2b_256.hash) ]
    else []
  in
  Bench.Test.create_group ~name:"Blake2b"
    (List.map
       (fun (name, (hash_function : bytes -> bytes -> bytes -> unit)) ->
         Bench.Test.create_with_initialization ~name
           ((fun size `init ->
              let msg = gen size in
              fun () -> ignore (hash_function Bytes.empty msg digest))
              hash_msg_size))
       tests)

let hash_evercrypt_tests =
  let msg = gen hash_msg_size in
  let key = gen 32 in
  Bench.Test.create_group ~name:"EverCrypt"
    (List.map
       (fun (name, alg) ->
         let digest = Bytes.create (SharedDefs.HashDefs.digest_len alg) in
         Bench.Test.create_group ~name
           [
             Bench.Test.create ~name:"direct" (fun () ->
                 ignore (EverCrypt.Hash.hash alg digest msg));
             Bench.Test.create ~name:"HMAC" (fun () ->
                 ignore (EverCrypt.HMAC.mac alg digest key msg));
           ])
       [
         ("SHA256", SharedDefs.HashDefs.SHA2_256);
         ("SHA512", SharedDefs.HashDefs.SHA2_512);
       ])

let hash_hacl_tests =
  Bench.Test.create_group ~name:"HACL*"
    (List.map
       (fun (name, digest_size, hash_function) ->
         Bench.Test.create_group ~name
           [
             Bench.Test.create_with_initialization ~name:"direct"
               ((fun msg_size digest_size `init ->
                  let msg = gen msg_size in
                  let digest = Bytes.create digest_size in
                  fun () -> ignore (hash_function msg digest))
                  hash_msg_size digest_size);
           ])
       [
         ("SHA3-256", 32, Hacl.SHA3_256.hash);
         ("SHA3-512", 64, Hacl.SHA3_512.hash);
         ("Keccak256", 32, Hacl.Keccak.keccak 1088 512 1);
       ])

let nacl_tests =
  let pt = gen hash_msg_size in
  let pk = gen 32 in
  let sk = gen 32 in
  let n = gen 24 in
  let ck = Bytes.create 32 in
  assert (Hacl.NaCl.box_beforenm ck pk sk);
  let ct = Bytes.create (hash_msg_size + 16) in
  assert (Hacl.NaCl.Easy.box_afternm ct pt n ck);
  let ct_detached = Bytes.copy pt in
  let tag = Bytes.create 16 in
  let key = gen 32 in
  assert (Hacl.NaCl.Detached.box_afternm ct_detached tag pt n ck);
  let open Bench.Test in
  create_group ~name:"NaCl"
    [
      create ~name:"box_beforenm" (fun () ->
          ignore (Hacl.NaCl.box_beforenm ck pk sk));
      (* functions moved in Noalloc submodule in version 0.4, using new names here
       * in order to correctly aggregate the results *)
      create ~name:"Noalloc.Easy.box_afternm" (fun () ->
          assert (Hacl.NaCl.Easy.box_afternm ct pt n ck));
      create ~name:"Noalloc.Easy.box_open_afternm" (fun () ->
          assert (Hacl.NaCl.Easy.box_open_afternm pt ct n ck));
      create ~name:"Noalloc.Detached.box_afternm" (fun () ->
          assert (Hacl.NaCl.Detached.box_afternm ct_detached tag pt n ck));
      create ~name:"Noalloc.Detached.box_open_afternm" (fun () ->
          assert (Hacl.NaCl.Detached.box_open_afternm pt ct_detached tag n ck));
      create ~name:"Noalloc.Easy.secretbox" (fun () ->
          assert (Hacl.NaCl.Easy.secretbox ct pt n key));
      create ~name:"Noalloc.Easy.secretbox_open" (fun () ->
          assert (Hacl.NaCl.Easy.secretbox_open pt ct n key));
    ]

let ed25519_tests =
  let sk_size = 32 in
  let pk_size = 32 in
  let sig_size = 64 in
  let msg = gen sign_msg_size in
  let sk = gen sk_size in
  let pk = Bytes.create pk_size in
  Hacl.Ed25519.secret_to_public pk sk;
  let signature = Bytes.create sig_size in
  Hacl.Ed25519.sign signature sk msg;
  let dummy_sk = Bytes.copy sk in
  let dummy_signature = Bytes.copy signature in
  let open Bench.Test in
  create_group ~name:"Ed25519"
    [
      create ~name:"secret_to_public" (fun () ->
          Hacl.Ed25519.secret_to_public dummy_sk pk);
      create ~name:"sign" (fun () -> Hacl.Ed25519.sign dummy_signature sk msg);
      create ~name:"verify" (fun () ->
          assert (Hacl.Ed25519.verify pk msg signature));
    ]

let curve25519_tests =
  let sk = gen 32 in
  let pk = Bytes.create 32 in
  let tests =
    [ ("51", Hacl.Curve25519_51.secret_to_public) ]
    @
    if AutoConfig2.(has_feature AVX2) then
      [ ("64", Hacl.Curve25519_64.secret_to_public) ]
    else []
  in
  Bench.Test.create_group ~name:"Curve25519"
    (List.map
       (fun (name, func) ->
         Bench.Test.create ~name (fun () -> ignore (func pk sk)))
       tests)

let p256_tests =
  let msg = gen sign_msg_size in
  let sk_size = 32 in
  let pk_size = 64 in
  let sig_size = 64 in
  let gen_key_sig () =
    let rec get_valid_sk () =
      let sk = gen sk_size in
      if Hacl.P256.valid_sk sk then sk else get_valid_sk ()
    in
    let hacl_p256_keypair () =
      let sk = get_valid_sk () in
      let pk = Bytes.create pk_size in
      assert (Hacl.P256.dh_initiator pk sk);
      (sk, pk)
    in
    let sk, pk = hacl_p256_keypair () in
    let k = get_valid_sk () in
    let signature = Bytes.create sig_size in
    assert (Hacl.P256.sign sk msg k signature);
    (pk, sk, signature)
  in

  let p256_hacl =
    let pk, sk, signature = gen_key_sig () in
    let pk_compressed = Bytes.create 33 in
    let pk_uncompressed = Bytes.create 65 in
    Hacl.P256.compress_c pk pk_compressed;
    Hacl.P256.compress_n pk pk_uncompressed;
    let open Bench.Test in
    create_group ~name:"P-256"
      [
        create ~name:"verify" (fun () ->
            assert (Hacl.P256.verify pk msg signature));
        create ~name:"dh_initiator" (fun () ->
            assert (Hacl.P256.dh_initiator pk sk));
        create ~name:"valid_sk" (fun () -> assert (Hacl.P256.valid_sk sk));
        create ~name:"valid_pk" (fun () -> assert (Hacl.P256.valid_pk pk));
        create ~name:"raw_to_compressed" (fun () ->
            ignore (Hacl.P256.compress_c pk pk_compressed));
        create ~name:"raw_to_uncompressed" (fun () ->
            ignore (Hacl.P256.compress_n pk pk_uncompressed));
        create ~name:"compressed_to_raw" (fun () ->
            ignore (Hacl.P256.decompress_c pk_compressed pk));
        create ~name:"uncompressed_to_raw" (fun () ->
            ignore (Hacl.P256.decompress_n pk_uncompressed pk));
      ]
  in

  (* Runs the P-256 verfication function from the lower-level hacl-star-raw package
     directly, which is what hacl-star does under the hood *)
  let p256_hacl_ctypes =
    let module Hacl_P256 = Hacl_P256_bindings.Bindings (Hacl_P256_stubs) in
    let p256_pk, _, p256_signature = gen_key_sig () in
    let size_uint32 b = Unsigned.UInt32.of_int (Bytes.length b) in
    let ctypes_buf = Ctypes.ocaml_bytes_start in
    let r, s =
      (Bytes.sub p256_signature 0 32, Bytes.sub p256_signature 32 32)
    in
    let size_msg = size_uint32 msg in
    let b_msg = ctypes_buf msg in
    let b_p256_pk = ctypes_buf p256_pk in
    let b_r = ctypes_buf r in
    let b_s = ctypes_buf s in
    Bench.Test.create ~name:"verify (hacl-star-raw)" (fun () ->
        assert (
          Hacl_P256.hacl_P256_ecdsa_verif_without_hash size_msg b_msg b_p256_pk
            b_r b_s))
  in
  Bench.Test.create_group ~name:"P-256" [ p256_hacl; p256_hacl_ctypes ]

let tests =
  [
    p256_tests;
    ed25519_tests;
    blake2b_tests;
    hash_hacl_tests;
    hash_evercrypt_tests;
    curve25519_tests;
    nacl_tests;
  ]

let main () =
  let open Bench in
  let run_config =
    Run_config.create ~verbosity:Quiet ~quota:(Quota.of_string "2") ()
  in
  let measurements = measure ~run_config tests in
  let analyses =
    List.map
      (fun m ->
        match analyze m with
        | Ok a -> a
        | Error _ -> failwith "Analysis failure")
      measurements
  in
  let display_config = Display_config.create ~show_output_as_sexp:true () in
  display ~display_config analyses

let () =
  Gc.(
    set { (get ()) with allocation_policy = 2 (* Use the best-fit strategy *) });
  main ()
