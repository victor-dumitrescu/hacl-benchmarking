(* The MIT License (MIT)
 *
 *   Copyright (c) 2021-2023 Nomadic Labs <contact@nomadic-labs.com>
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE. *)

open Core_bench
open Hacl_star

let default_msg_size = 10000
let hash_msg_sizes = [100; 10000; 100000]
let sign_msg_size = 256

let gen size =
  match Hacl.RandomBuffer.randombytes ~size with
  | Some b -> b
  | None -> failwith "Error getting random bytes"

let blake2b_tests =
  let tests =
    [ ("32", Hacl.Blake2b_32.hash) ]
    @
    if AutoConfig2.(has_feature VEC256) then [ ("256", Hacl.Blake2b_256.hash) ]
    else []
  in
  Bench.Test.create_group ~name:"Blake2b"
    (List.flatten (List.map
       (fun (name, (hash_function : ?key:bytes -> bytes -> int -> bytes)) ->
          List.map (fun msg_size ->
             let name = Printf.sprintf "%s/%d" name msg_size in
         Bench.Test.create_with_initialization ~name
           ((fun size digest_size `init ->
              let msg = gen size in
              fun () -> ignore (hash_function msg digest_size))
              msg_size 64)) hash_msg_sizes)
       tests))

let hash_evercrypt_tests =
  let msg = gen default_msg_size in
  let key = gen 32 in
  Bench.Test.create_group ~name:"EverCrypt"
    (List.map
       (fun (name, alg) ->
         Bench.Test.create_group ~name
           [
             Bench.Test.create ~name:"direct" (fun () ->
                 ignore (EverCrypt.Hash.hash ~alg ~msg));
             Bench.Test.create ~name:"HMAC" (fun () ->
                 ignore (EverCrypt.HMAC.mac ~alg ~key ~msg));
           ])
       [
         ("SHA256", SharedDefs.HashDefs.SHA2_256);
         ("SHA512", SharedDefs.HashDefs.SHA2_512);
       ])

let hash_hacl_tests =
  Bench.Test.create_group ~name:"HACL*"
    (List.map
       (fun (name, hash_function) ->
         Bench.Test.create_group ~name
           [
             Bench.Test.create_with_initialization ~name:"direct"
               ((fun size `init ->
                  let msg = gen size in
                  fun () -> ignore (hash_function msg))
                  default_msg_size);
           ])
       [
         ("SHA3-256", Hacl.SHA3_256.hash);
         ("SHA3-512", Hacl.SHA3_512.hash);
         ( "Keccak256",
           fun msg ->
             Hacl.Keccak.keccak ~rate:1088 ~capacity:512 ~suffix:1 ~size:32 ~msg
         );
       ])

let nacl_tests =
  let pt = gen default_msg_size in
  let pk = gen 32 in
  let sk = gen 32 in
  let n = gen 24 in
  let ck = Option.get (Hacl.NaCl.box_beforenm ~pk ~sk) in
  let combined_ct = Option.get (Hacl.NaCl.box_afternm ~pt ~n ~ck) in
  let tag, _ =
    (Bytes.sub combined_ct 0 16, Bytes.sub combined_ct 16 default_msg_size)
  in
  let key = gen 32 in
  let buf = Bytes.copy pt in
  let open Bench.Test in
  create_group ~name:"NaCl"
    [
      create ~name:"box_beforenm" (fun () ->
          ignore (Hacl.NaCl.box_beforenm ~pk ~sk));
      create ~name:"Noalloc.Easy.box_afternm" (fun () ->
          assert (
            Hacl.NaCl.Noalloc.Easy.box_afternm ~pt ~n ~ck ~ct:combined_ct ()));
      create ~name:"Noalloc.Easy.box_open_afternm" (fun () ->
          assert (
            Hacl.NaCl.Noalloc.Easy.box_open_afternm ~ct:combined_ct ~n ~ck ~pt
              ()));
      create ~name:"Noalloc.Detached.box_afternm" (fun () ->
          assert (Hacl.NaCl.Noalloc.Detached.box_afternm ~buf ~n ~ck ~tag ());
          assert (
            Hacl.NaCl.Noalloc.Detached.box_open_afternm ~buf ~n ~ck ~tag ()));
      create ~name:"Noalloc.Easy.secretbox" (fun () ->
          assert (Hacl.NaCl.Noalloc.Easy.secretbox ~pt ~n ~key ~ct:combined_ct ()));
      create ~name:"Noalloc.Easy.secretbox_open" (fun () ->
          assert (
            Hacl.NaCl.Noalloc.Easy.secretbox_open ~pt ~n ~key ~ct:combined_ct ()));
    ]

let curve25519_tests =
  let sk = gen 32 in
  let tests =
    [ ("51", Hacl.Curve25519_51.secret_to_public) ]
    @
    if AutoConfig2.(has_feature VEC256) then
      [ ("64", Hacl.Curve25519_64.secret_to_public) ]
    else []
  in
  Bench.Test.create_group ~name:"Curve25519"
    (List.map
       (fun (name, func) ->
         Bench.Test.create ~name (fun () -> ignore (func ~sk)))
       tests)

let ed25519_tests =
  let sk = gen 32 in
  let msg = gen sign_msg_size in
  let pk = Hacl.Ed25519.secret_to_public ~sk in
  let signature = Hacl.Ed25519.sign ~sk ~msg in
  let open Bench.Test in
  create_group ~name:"Ed25519"
    [
      create ~name:"secret_to_public" (fun () ->
          ignore (Hacl.Ed25519.secret_to_public ~sk));
      create ~name:"sign" (fun () -> ignore (Hacl.Ed25519.sign ~sk ~msg));
      create ~name:"verify" (fun () ->
          assert (Hacl.Ed25519.verify ~pk ~msg ~signature));
    ]

let p256_tests =
  let msg = gen sign_msg_size in
  let gen_key_sig () =
    let sk_size = 32 in
    let rec get_valid_sk () =
      let sk = gen sk_size in
      if Hacl.P256.valid_sk ~sk then sk else get_valid_sk ()
    in
    let hacl_p256_keypair () =
      let sk = get_valid_sk () in
      let pk_of_sk sk = Hacl.P256.dh_initiator ~sk in
      match pk_of_sk sk with
      | Some pk -> (sk, pk)
      | None -> failwith "P256.keypair: failure"
    in
    let sk, pk = hacl_p256_keypair () in
    let k = get_valid_sk () in
    match Hacl.P256.sign ~sk ~msg ~k with
    | Some signature -> (pk, sk, signature)
    | None -> assert false
  in

  let p256_hacl =
    let pk, sk, signature = gen_key_sig () in
    let pk_compressed = Hacl.P256.raw_to_compressed pk in
    let pk_uncompressed = Hacl.P256.raw_to_uncompressed pk in
    let open Bench.Test in
    create_group ~name:"P-256"
      [
        create ~name:"verify" (fun () ->
            assert (Hacl.P256.verify ~pk ~msg ~signature));
        create ~name:"dh_initiator" (fun () ->
            ignore (Hacl.P256.dh_initiator ~sk));
        create ~name:"valid_sk" (fun () -> assert (Hacl.P256.valid_sk ~sk));
        create ~name:"valid_pk" (fun () -> assert (Hacl.P256.valid_pk ~pk));
        create ~name:"raw_to_compressed" (fun () ->
            ignore (Hacl.P256.raw_to_compressed pk));
        create ~name:"raw_to_uncompressed" (fun () ->
            ignore (Hacl.P256.raw_to_uncompressed pk));
        create ~name:"compressed_to_raw" (fun () ->
            ignore (Hacl.P256.compressed_to_raw pk_compressed));
        create ~name:"uncompressed_to_raw" (fun () ->
            ignore (Hacl.P256.uncompressed_to_raw pk_uncompressed));
      ]
  in

  (* Runs the P-256 verfication function from the lower-level hacl-star-raw package
     directly, which is what hacl-star does under the hood *)
  let p256_hacl_ctypes =
    let module Hacl_P256 = Hacl_P256_bindings.Bindings (Hacl_P256_stubs) in
    let p256_pk, _, p256_signature = gen_key_sig () in
    let size_uint32 b = Unsigned.UInt32.of_int (Bytes.length b) in
    let ctypes_buf = Ctypes.ocaml_bytes_start in
    let r, s =
      (Bytes.sub p256_signature 0 32, Bytes.sub p256_signature 32 32)
    in
    let size_msg = size_uint32 msg in
    let b_msg = ctypes_buf msg in
    let b_p256_pk = ctypes_buf p256_pk in
    let b_r = ctypes_buf r in
    let b_s = ctypes_buf s in
    Bench.Test.create ~name:"verify (hacl-star-raw)" (fun () ->
        assert (
          Hacl_P256.hacl_P256_ecdsa_verif_without_hash size_msg b_msg b_p256_pk
            b_r b_s))
  in
  Bench.Test.create_group ~name:"P-256" [ p256_hacl; p256_hacl_ctypes ]

#if HACL_STAR_MINOR >= 7
module Secp256k1 = struct
  let sk_size = 32
  (* from hacl-star tests *)
  let rec get_valid_k () =
    let k = gen 32 in
    if Hacl.K256.valid_sk ~sk:k then k else get_valid_k ()

  let generate_key seed =
    let sk = if Hacl.K256.valid_sk ~sk:seed then seed else get_valid_k () in
    match Hacl.K256.secret_to_public ~sk with
    | Some pk ->
      assert (Hacl.K256.valid_pk ~pk);
      (pk, sk)
    | None -> assert false

  let secp256k1_hacl =
    let msg = EverCrypt.Hash.hash ~alg:SharedDefs.HashDefs.BLAKE2s ~msg:(gen sign_msg_size) in
    let k = get_valid_k () in
    let pk, sk = generate_key (gen sk_size) in
    let signature = Option.get @@ Hacl.K256.Libsecp256k1.sign ~sk ~msg ~k in
    let test_sign =
      Bench.Test.create ~name:"sign" (fun () ->
          ignore (Hacl.K256.Libsecp256k1.sign ~sk ~msg ~k)) in
    let test_verify =
      Bench.Test.create ~name:"verify" (fun () ->
          assert (Hacl.K256.Libsecp256k1.verify ~pk ~msg ~signature)) in
    Bench.Test.create_group ~name:"Libsecp256k1" [ test_sign; test_verify ]

  let secp256k1_internal =
    let open Libsecp256k1.External in
    let gen_bigstring size = Bigstring.of_bytes (gen size) in
    let msg = gen sign_msg_size in
    let context =
      let ctx = Context.create () in
      match Context.randomize ctx (gen_bigstring sk_size) with
      | false -> failwith "Secp256k1 context randomization failed. Aborting."
      | true -> ctx
    in
    let sk = Key.read_sk_exn context (gen_bigstring sk_size) in
    let pk = Key.neuterize_exn context sk in
    let signature = Sign.sign_exn context ~sk (Bigstring.of_bytes msg) in
    let secp256k1_sign =
      Bench.Test.create ~name:"sign" (fun () ->
          ignore (Sign.sign_exn context ~sk (Bigstring.of_bytes msg)))
    in
    let secp256k1_verify =
      Bench.Test.create ~name:"verify" (fun () ->
          assert (
            Sign.verify_exn context ~pk ~msg:(Bigstring.of_bytes msg) ~signature))
    in
    Bench.Test.create_group ~name:"secp256k1-internal"
      [ secp256k1_sign; secp256k1_verify ]
end
#endif

let tests =
  [
    p256_tests;
    ed25519_tests;
    blake2b_tests;
    hash_hacl_tests;
    hash_evercrypt_tests;
    curve25519_tests;
#if HACL_STAR_MINOR >= 7
    Secp256k1.secp256k1_hacl;
#if HACL_STAR_PATCH = 0
    Secp256k1.secp256k1_internal; (* only once per benchmark run *)
#endif
#endif
    nacl_tests;
  ]

let main () =
  let open Bench in
  let run_config =
    Run_config.create ~verbosity:Quiet ~quota:(Quota.of_string "2") ()
  in
  let measurements = measure ~run_config tests in
  let analyses =
    List.map
      (fun m ->
        match analyze m with
        | Ok a -> a
        | Error _ -> failwith "Analysis failure")
      measurements
  in
  let display_config = Display_config.create ~show_output_as_sexp:true () in
  display ~display_config analyses

let () =
  Gc.(
    set { (get ()) with allocation_policy = 2 (* Use the best-fit strategy *) });
  main ()
