# hacl-benchmarking
Tool for benchmarking and comparing performance across versions of the `hacl-star` package

# Usage
- if running on a fresh machine, install `opam` and run `opam init --bare`
- run `./script/install_deps.sh` to create opam switch and install dependencies
- define which versions to compare in `run_benchmark.sh` (which can also include local, unpublished versions) and run script
- raw results are written in `results_sexp.txt` and are then aggregated using `utils/analyse.ml`
- more detailed benchmark results for BLAKE2b in `results_blake.txt`
