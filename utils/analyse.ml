(* The MIT License (MIT)
 *
 *   Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE. *)

open Core

let bench_name = "benchmark_name_with_index"

let bench_time = "time_per_run_nanos"

let find_in_sexp name = function
  | Sexp.List [ Atom n; Atom v ] -> if String.equal n name then Some v else None
  | _ -> None

(* round and change unit from ns to us *)
let round_time time =
  Float.of_string time /. 1000.
  |> Float.round_nearest |> Float.to_int |> Int.to_string

let versions = ref (Set.empty (module String))

let get_name_and_time sexp =
  match sexp with
  | Sexp.List results ->
      let name = List.filter_map ~f:(find_in_sexp bench_name) results in
      let time = List.filter_map ~f:(find_in_sexp bench_time) results in
      assert (List.length name = 1 && List.length time = 1);
      (List.hd_exn name, round_time (List.hd_exn time))
  | _ -> failwith "Unexpected format"

let process_benchmark results_map = function
  | Sexp.List
      [
        Sexp.List [ Atom "hacl_version"; Atom hacl_version ];
        Sexp.List benchmark;
      ] ->
      versions := Set.add !versions hacl_version;
      let results = List.map ~f:get_name_and_time benchmark in
      List.fold_left ~init:results_map
        ~f:(fun results (name, time) ->
          Map.change results name ~f:(fun v ->
              match v with
              | None -> Some [ (hacl_version, time) ]
              | Some l -> Some ((hacl_version, time) :: l)))
        results
  | _ -> failwith "Unexpected format"

let print_md_table filename =
  match Parsexp_io.load (module Parsexp.Many) ~filename with
  | Ok benchmarks ->
      let benchmarks =
        List.fold_left
          ~init:(Map.empty (module String))
          ~f:process_benchmark benchmarks
      in
      let versions = Set.elements !versions in
      Printf.printf "| Primitive | %s |\n" (String.concat ~sep:" | " versions);
      let border =
        String.concat ~sep:"|"
          (List.init (List.length versions + 1) ~f:(fun _ -> "---"))
      in
      Printf.printf "|%s|\n" border;
      Map.iteri
        ~f:(fun ~key ~data ->
          let data = Map.of_alist_exn (module String) data in
          let data =
            List.map versions ~f:(fun k ->
                match Map.find data k with Some v -> v | None -> "")
          in
          Printf.printf "%s |\n" (String.concat ~sep:" | " (key :: data)))
        benchmarks
  | Error error ->
      Parsexp.Parse_error.report Caml.Format.std_formatter error ~filename

let () = print_md_table "results_sexp.txt"
