#! /bin/sh

set -e

ocaml_version=4.14.1

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"

rm -fR _opam _build
opam repository set-url default https://opam.ocaml.org
opam update
opam switch create "$src_dir" ocaml-base-compiler.$ocaml_version --no-install

opam install -y odoc merlin zarith hex digestif parsexp_io cppo secp256k1-internal

opam install . --deps-only -y
opam install hacl-star-raw --deps-only -y
eval $(opam env --shell=sh)
